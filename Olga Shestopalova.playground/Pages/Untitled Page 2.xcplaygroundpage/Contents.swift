//Домашнее задание №2. Задачи про молочника Ивана.

import Foundation


//Задача 1

var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

/* ----
Задача 2 */

let oldMilkPrice: Int = 3

/* ----
Задача 3 */

var milkPrice: Double = 4.20

/* ----
Задача 4 */

var milkBottleCount: Int? = 20
var profit: Double? = 0.0
profit = milkPrice * Double(milkBottleCount!)
print("Выручка от продажи молока составила ", profit!, " рублей.")

/* ----
Задача 5 */

var employeesList: [String] = []

employeesList = ["Иван Лактозов", "Петр", "Геннадий"]

/* ----
Задача 6 */

var isEveryoneWorkHard: Bool = false
var workingHours: Int = 38

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false}
print("Все ли сотрудники работают добросовестно? Ответ: ", isEveryoneWorkHard)

