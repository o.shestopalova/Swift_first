/*
 Есть неотсортированный список произвольных фамилий студентов, например: Акулова, Валерьянов, Бочкин, Бабочкина, Арбузов, Васильева и тд.
 Что нужно сделать: Создать класс, способный хранить и выводить упорядоченный список фамилий студентов отсортированных по первой букве их фамилии.

 Пример:

 А

 Акулова

 Арбузов

 Б

 Бочкин

 Бабочкина

 В

 Валерьянов

 Васильева

 ...

 Требования: Структура должна поддерживать возможность добавления фамилий и вывод всего списка упорядоченных фамилий в консоль. Разрешается использовать массивы (Array), множества (Set) и словари (Dictionary).

 Рекомендации
     Строка (String) представляет собой упорядоченную коллекцию символов (Character).

 Пример проверки работоспособности:

 let list = NameList()
 list.addName(name: "Акулова")
 list.addName(name: "Валерьянов")
 list.addName(name: "Бочкин")
 list.addName(name: "Бабочкина")
 list.addName(name: "Арбузов")
 list.addName(name: "Васильева")
 list.printNames()
*/

class NameList {
    var nameList: [Character: [String]] = [:]
    
    func addName(name: String) {
        let firstLetter = name.first ?? " "
        
        if nameList[firstLetter] != nil {
            nameList[firstLetter]?.append(name)
        } else {
            nameList[firstLetter] = [name]
        }
    }

    
    func printNames() {
        let sortedKeys = nameList.keys.sorted()
        
        for key in sortedKeys {
            print("\(key)")
            print("\n")
            let sortedNames = nameList[key] ?? []
            for name in sortedNames {
                print(name)
                print("\n")
            }
        }
    }
}

    
let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.printNames()
