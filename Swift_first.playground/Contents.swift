import Foundation

/*Дан код:
 
 enum Device {
     case iPhone14Pro
     case iPhoneXR
     case iPadPro
      
     var name: String {
         switch self {
         case .iPhone14Pro: return "iPhone 14 Pro"
         case .iPhoneXR: return "iPhone XR"
         case .iPadPro: return "iPad Pro"
         }
     }
      
     var screenSize: CGSize {
         switch self {
         case .iPhone14Pro: return CGSize(width: 393, height: 852)
         case .iPhoneXR: return CGSize(width: 414, height: 896)
         case .iPadPro: return CGSize(width: 834, height: 1194)
         }
     }
      
     var screenDiagonal: Double {
         switch self {
         case .iPhone14Pro: return 6.1
         case .iPhoneXR: return 6.06
         case .iPadPro: return 11
         }
     }
      
     var scaleFactor: Int {
         switch self {
         case .iPhone14Pro:
             return 3
         case .iPhoneXR, .iPadPro:
             return 2
         }
     }
      
     func physicalSize() -> CGSize {
         return CGSize(
             width: screenSize.width * CGFloat(scaleFactor),
             height: screenSize.height * CGFloat(scaleFactor)
         )
     }
 }

 Что нужно сделать:

     Переписать Device с перечисления на структуру так, чтобы все свойства (name, screenSize, screenDiagonal, ...) и функция (physicalSize) сохранились.
     Постарайтесь сохранить порядок вызова функции Device.iPhoneXR.physicalSize(), чтобы после переписывания на структуру, мы смогли бы также обратиться к функции через Device.iPhoneXR.physicalSize().*/


struct Device {
    static let iPhone14Pro = Device(name: "iPhone 14 Pro", screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    /* 1) Использую именованные кортежи, т.к. можем обратиться к каждому элементу по имени.
     2) Использую static let, чтобы получать доступ к свойству через тип Device. Тут уже интереснее получается, т.к. мы создали тип по сути и перед скобкой ставим Device как для экземпляра структуры. И вот здесь хотелось бы человеческого разъяснения когда четко используем кортеж, а когда структуру, т.к. на текущий момент нет однозначного понимания =)
     3) Также использую вложенность в кортеже (один в другом) для параметра screenSize с типом CGSize, т.к. он состоит из ширины и высоты экрана */
    static let iPhoneXR = Device(name: "iPhone XR", screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    static let iPadPro = Device(name: "iPad Pro", screenSize: CGSize(width: 834, height: 1194), screenDiagonal: 11, scaleFactor: 2)
    
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

print("Параметры выбранного устройства в формате 'ширина, высота': \(Device.iPhoneXR.physicalSize())")
